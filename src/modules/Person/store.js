export const SET_PERSONS = 'person/SET_PERSONS'
export const SET_PERSON = 'person/SET_PERSON'
export const CREATE_PERSON = 'person/CREATE_PERSON'
export const UPDATE_PERSON = 'person/UPDATE_PERSON'
export const DELETE_PERSON = 'person/DELETE_PERSON'
export const SET_ERRORS = 'person/SET_ERRORS'

import { PersonServices } from './services'

export const PersonStore = {
  namespaced: true,
  state: {
    persons: null,
    person: null,
    errors: null,
  },
  getters: {
    personsList(state) {
      return state.persons
    },
    personItem(state) {
      return state.person
    },
    errorsList(state) {
      return state.errors
    }
  },
  actions: {
    getAllPersons: async ({ commit }) => {
      const res = await PersonServices.getAllPersons()
      commit(SET_PERSONS, res.data)
    },
    getOnePerson: async({ commit }, personId) => {
      const res = await PersonServices.getOnePerson(personId)
      commit(SET_PERSON, res.data)
    },
    createPerson: async ({ commit }, person) => {
      try {
        const res = await PersonServices.createPerson(person)
        commit(CREATE_PERSON, res.data)
        commit(SET_ERRORS, null)
      } catch (err) {
        commit(SET_ERRORS, err.response.data)
      }
    },
    updatePerson: async ({ commit }, payload) => {
      try {
        await PersonServices.updatePerson(payload)
        commit(SET_ERRORS, null)
      } catch (err) {
        commit(SET_ERRORS, err.response.data.errors)
      }
    },
    deletePerson: async ({ commit }, personId) => {
      await PersonServices.deletePerson(personId)
      commit(DELETE_PERSON, personId)
    }
  },
  mutations: {
    [SET_PERSONS](state, payload) {
      state.persons = payload
    },
    [SET_PERSON](state, payload) {
      state.person = payload
    },
    [CREATE_PERSON](state, payload) {
      state.persons.data.push(payload)
    },
    [UPDATE_PERSON](state, payload) {
      const newState = state.persons.data.map(person => {
        if (person.id == payload.id) person = payload

        return person
      })

      state.persons.data = newState
    },
    [DELETE_PERSON](state, payload) {
      state.persons = state.persons.data.filter(person => person.id != payload)
    },
    [SET_ERRORS](state, payload) {
      state.errors = payload
    }
  }
}
