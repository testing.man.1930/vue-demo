import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import { PersonStore } from '@/modules/Person/store'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Person: { ...PersonStore }
  }
})
