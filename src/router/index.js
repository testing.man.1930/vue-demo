import Vue from 'vue'
import VueRouter from 'vue-router'

// Modules
import { PersonRoutes } from '@/modules/Person/routes'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/person'
  },
  {
    path: '/datatable',
    component: () => import('@/components/Datatable.vue')
  },
  ...PersonRoutes
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
