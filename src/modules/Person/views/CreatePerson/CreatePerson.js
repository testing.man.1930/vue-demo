import { mapActions, mapGetters } from 'vuex'

import InputField from '@/components/InputField'

export default {
  name: 'CreatePerson',
  components: {
    InputField
  },
  data() {
    return {
      person: {
        name: '',
        lastname: '',
        motherlastname: '',
        gender: 'M',
        maritalstatus: '',
        birthdate: '',
        placebirth: 'Huancayo',
        typedocument: 'DNI',
        numberdocument: ''
      }
    }
  },
  methods: {
    ...mapActions('Person', ['createPerson']),
    createNewPerson() {
      this.createPerson(this.person)

      this.$router.push({ name: 'person.index' })
    }
  },
  computed: {
    ...mapGetters('Person', ['errorsList'])
  }
}
