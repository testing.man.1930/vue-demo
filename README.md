# Vuejs DEMO

## Configuración del Proyecto
```
npm install
```

### Compilar en Desarrollo
```
npm run serve
```
### Variables de Entorno

- Cambiar ```.env.example``` a ```.env.local```
- Poner la URL de la API en la variable```VUE_APP_API_URL```
