export function lazyLoad(view) {
  return () => import(`./views/${view}.vue`)
}
