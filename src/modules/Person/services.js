import { httpClient } from '@/plugins/axios'

export const PersonServices = {
  getAllPersons() {
    return httpClient.get('/v1/person')
  },
  getOnePerson(personId) {
    return httpClient.get(`/v1/person/${personId}`)
  },
  createPerson(person) {
    return httpClient.post('/v1/person', person)
  },
  updatePerson(payload) {
    const { personId, person } = payload
    return httpClient.put(`/v1/person/${personId}`, person)
  },
  deletePerson(personId) {
    return httpClient.delete(`/v1/person/${personId}`)
  },
}
