import CreatePerson from './views/CreatePerson/CreatePerson.vue'
import UpdatePerson from './views/UpdatePerson'
const ShowAllPersons = () => import('./views/ShowAllPersons')

export const PersonRoutes = [
  {
    path: '/person',
    name: 'person.index',
    component: ShowAllPersons
  },
  {
    path: '/person/create',
    name: 'person.create',
    component: CreatePerson
  },
  {
    path: '/person/edit/:personId',
    name: 'person.edit',
    component: UpdatePerson
  }
]
